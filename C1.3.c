#include <stdio.h>

int main()
{
int x,y,z ;
printf("Enter integer no 1:");
scanf("%d",&x);
printf("Enter integer no 2:");
scanf("%d",&y);
printf("Before Swapping \n Integer no 1 = %d \n Integer no 2 = %d\n",x,y);
z=x;
x=y;
y=z;
printf("After Swapping \n Integer no 1 = %d \n Integer no 2 = %d \n ",x,y);
return 0;
}
